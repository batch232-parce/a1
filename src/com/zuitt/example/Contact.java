package com.zuitt.example;

public class Contact {
    // Properties
    private String name;
    private String contactNumber;
    private String address;

    // Constructors
    public Contact (String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public Contact (){
    };

    // Getters
    public String getName(){
        return this.name;
    }
    public String getContact(){
        return this.contactNumber;
    }
    public String getAddress(){
        return this.address;
    }

    // Setters
    public void setName(String name){
        this.name = name;
    }
    public void setContact(String contactNumber){
        this.contactNumber = contactNumber;
    }
    public void setAddress(String address){
        this.address = address;
    }

    public String toString(){
        return this.name + " \n" + this.name + " has the following registered number: " + "\n" + this.contactNumber + "\n" +
                this.name + " has the following registered address: " + "\n" + this.address + "\n";
    }

}
