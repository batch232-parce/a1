package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook{
    // Property
    private ArrayList<Contact> contacts = new ArrayList<>();
    private Contact contact = new Contact();

    // Constructor
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }
    public Phonebook(){};

    // Getter
    public ArrayList<Contact> getContactList(){
        return this.contacts;
    }

    // Setter
    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }
}
