import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Phonebook myContact = new Phonebook();

        Contact myContact1 = new Contact("Ada Lovelace", "+639467163353", "Marylebone, London, United Kingdom");
        Contact myContact2 = new Contact("Dennis Ritchie", "+639308802195", "Bronxville, New York, United States");

        myContact.setContacts(myContact1);
        myContact.setContacts(myContact2);

        ArrayList<Contact> listOfContacts = myContact.getContactList();

        if (listOfContacts.isEmpty()) {
            System.out.println("Contact list is empty");
        }
        else {
            for (Contact contact : listOfContacts) {
                System.out.println(contact);
            }
        }
    }
}